public class Student{
	int grade;
	String name;
	double absenceTime;
	
	public Student(int x, String y, double a){
		grade = x;
		name = y;
		absenceTime= a;
	    
	}
	public String sayHi(){
		return "Hi, my name is "+name+", let's have fun this semester";
	}
	public void pleadRemake(){
		System.out.println("Please sir, i have only been absent "+absenceTime+" hours, and i have a "+grade+" grade. Can i get a remake?");
		if(grade <90 && absenceTime<30.00){
			System.out.println("Too late buddy");
		}else{
			System.out.println("OKAY");
		}
	}
}